use std::fmt::format;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::process::Command;
use fancy_regex::Regex;
use std::env;

trait PackageManager {
    fn provides(file: &str) -> Vec<String>;
    fn transform_cmake(cmake_package: &str) -> String;
    fn transform_pkgconfig(cmake_package: &str) -> String;
    fn new() -> Self;
    fn extract_provides_information(output: &str) -> Option<String>;
}

struct ArchLinux {}

impl PackageManager for ArchLinux {
    fn new() -> Self {
        Self {}
    }

    fn provides(file: &str) -> Vec<String> {
        vec![
            "pacman".to_string(),
            "-F".to_string(),
            file.to_string()
        ]
    }

    fn transform_cmake(cmake_package: &str) -> String {
        format!("*/{}.cmake", cmake_package)
    }

    fn transform_pkgconfig(cmake_package: &str) -> String {
        format!("*/{}.pc", cmake_package)
    }

    fn extract_provides_information(output: &str) -> Option<String> {
        None
    }
}

struct Fedora {}

impl PackageManager for Fedora {
    fn new() -> Self {
        Self {}
    }

    fn provides(file: &str) -> Vec<String> {
        vec![
            "dnf".to_string(),
            "provides".to_string(),
            file.to_string()
        ]
    }

    fn transform_cmake(cmake_package: &str) -> String {
        format!("cmake({})", cmake_package)
    }

    fn transform_pkgconfig(cmake_package: &str) -> String {
        format!("pkgconfig({})", cmake_package)
    }

    fn extract_provides_information(output: &str) -> Option<String> {
        let re = Regex::new(r"[^\s]+(?=\s:\s)").unwrap();

        return Some(re.captures(output).unwrap().unwrap().get(0).unwrap().as_str().parse().unwrap());
    }
}

struct Debian {}

impl PackageManager for Debian {
    fn new() -> Self {
        Self {}
    }

    fn provides(file: &str) -> Vec<String> {
        vec![
            "dpkg".to_string(),
            "-S".to_string(),
            file.to_string()
        ]
    }
    fn transform_cmake(cmake_package: &str) -> String {
        format!("*/{}.cmake", cmake_package)
    }

    fn transform_pkgconfig(cmake_package: &str) -> String {
        format!("*/{}.pc", cmake_package)
    }

    fn extract_provides_information(output: &str) -> Option<String> {
        None
    }
}

struct OpenSUSE {}

impl PackageManager for OpenSUSE {
    fn new() -> Self {
        Self {}
    }

    fn provides(file: &str) -> Vec<String> {
        vec![
            "zypper".to_string(),
            "se".to_string(),
            "--provides".to_string(),
            "--match-exact".to_string(),
            file.to_string()
        ]
    }

    fn transform_cmake(cmake_package: &str) -> String {
        format!("cmake({})", cmake_package)
    }

    fn transform_pkgconfig(cmake_package: &str) -> String {
        format!("pkgconfig({})", cmake_package)
    }

    fn extract_provides_information(output: &str) -> Option<String> {
        None
    }
}

struct Gentoo {}

impl PackageManager for Gentoo {
    fn new() -> Self {
        Self {}
    }

    fn provides(file: &str) -> Vec<String> {
        vec![
            "e-file".to_string(),
            file.to_string()
        ]
    }

    fn transform_cmake(cmake_package: &str) -> String {
        format!("{}.cmake", cmake_package)
    }

    fn transform_pkgconfig(cmake_package: &str) -> String {
        format!("{}.pc", cmake_package)
    }

    fn extract_provides_information(output: &str) -> Option<String> {
        let re = Regex::new(r"(\s\*|\[I\])\s+([^\n]+)").unwrap();

        return if let Ok(captures) = re.captures(output) {
            if let Some(inner_captures) = captures {
                Some(inner_captures.get(2).unwrap().as_str().to_string())
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[derive(Debug)]
enum PackageType {
    CMakeModule,
    PkgConfigModule
}

#[derive(Debug)]
struct Package {
    package_type: PackageType,
    pub name: String
}

impl Package {
    pub fn newPkgConfig(name: &str) -> Self {
        Self {
            package_type: PackageType::PkgConfigModule,
            name: name.to_string()
        }
    }

    pub fn newCMakeModule(name: &str) -> Self {
        Self {
            package_type: PackageType::CMakeModule,
            name: name.to_string()
        }
    }
}

fn install_packages(package_list: &Vec<Package>) -> Vec<String> {
    let mut installable_packages: Vec<String> = Vec::new();

    // we must first find which packages provide these:
    for package in package_list {
        let transformed_name = match package.package_type {
            PackageType::CMakeModule => {
                Gentoo::transform_cmake(&package.name)
            }
            PackageType::PkgConfigModule => {
                Gentoo::transform_pkgconfig(&package.name)
            }
        };

        let provideCmd = Gentoo::provides(&transformed_name);

        let mut providesCommand = Command::new(&provideCmd[0]);

        let providesCommand = providesCommand.args(&provideCmd[1..]);

        let output = providesCommand.output()
            .expect("Failed to execute command");

        let output = String::from_utf8(output.stdout).unwrap();
        println!("output: {}", output);

        if let Some(package) = Gentoo::extract_provides_information(&output) {
            println!("FOUND PACKAGE TO INSTALL: {}", package);

            installable_packages.push(package.parse().unwrap());
        } else {
            println!("Failed to find package for {:#?}", package);
        }
    }

    return installable_packages;
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Usage: cmake-package-installer [path to cmake log]");
        return
    }

    let file = File::open(&args[1]).unwrap();
    let reader = BufReader::new(file);

    let mut package_list: Vec<Package> = vec![];

    for line in reader.lines() {
        let line = line.unwrap();
        if line.contains(" required by 'virtual:world', not found") {
            println!("Found pkgconfig error, parsing now...");

            println!("Line: {}", line);

            let re = Regex::new(r"--\s+Package\s\'(\w+)\'").unwrap();

            let package = &re.captures(&line).unwrap().unwrap().get(1).unwrap().as_str();
            println!("Found package: {}", package);

            package_list.push(Package::newPkgConfig(package));
        }

        if line.contains("Could not find a package configuration file provided") {
            println!("CMake error!");

            println!("Line: {}", line);

            let re = Regex::new(r#"\"(\w+)\""#).unwrap();

            let package = &re.captures(&line).unwrap().unwrap().get(1).unwrap().as_str();
            println!("Found package: {}", package);

            package_list.push(Package::newCMakeModule(package));
        }
    }

    println!("Found the following packages to install: {:#?}", package_list);

    println!("{:#?}", install_packages(&package_list));
}