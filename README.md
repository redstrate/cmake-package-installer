# cmake-package-installer

This is a solution to the eternal problem of figuring out what packages
to install, depending on what the CMake project requires.

The tool accomplishes this by parsing the CMake logs and then using the
distribution's existing packaging channels to lookup which packages provide
the necessary libraries, package configurations, etc.

**This tool is a proof of concept.**

## Design Goals

* Attempt to support every distribution supported by `kdesrc-build`:
  * Debian
  * OpenSUSE
  * Fedora
  * Mageia
  * Gentoo
  * Arch Linux
  * Alpine
  * FreeBSD
* Parse the following types of missing packages:
  * CMake packages
  * Pkgconfig packages
  * QML runtime dependencies

## Current Blockers

* The distribution MUST support grabbing arbitrary file information from packages
  - [ ] Debian (unknown)
  - [ ] OpenSUSE (unknown)
  - [X] Fedora (supported via `dnf provides`)
  - [ ] Mageia (unknown)
  - [ ] Gentoo (probably won't work, but there is a community database)
  - [ ] Arch Linux (think so)
  - [ ] Alpine (unknown)
  - [ ] FreeBSD (unknown)

## Architecture

1. The user runs a CMake build, probably through `kdesrc-build`.
2. The build fails, presumably due to missing packages.
3. `cmake-package-installer` parses the CMake log, and finds the probable missing packages that contain the required
files that CMake is looking for.
4. The package list is compiled and suggested to the user to install.